## ofxLo ##

openFrameworks addon for liblo OSC Library, inspired by ofxOsc.

Liblo 0.25 [http://liblo.sourceforge.net/](http://liblo.sourceforge.net/)

## features ##

- similar interface to ofxOsc
- send a message with pixels (using lo_blob)